<?php

echo json_encode([
    'items' => [
        [
            'city' => 'Valencia',
            'time' => "10:40",
            'temperature' => [
                'current' => 29,
                'high' => 26,
                'low' => 16
            ],
            'description' => "Sunny",
        ],
        [
            'city' => 'Bilbao',
            'time' => "10:40",
            'temperature' => [
                'current' => 21,
                'high' => 22,
                'low' => 12
            ],
            'description' => "Cloudy",
        ],
        [
            'city' => 'Madrid',
            'time' => "10:40",
            'temperature' => [
                'current' => 25,
                'high' => 26,
                'low' => 16
            ],
            'description' => "Sunny",
        ],
        [
            'city' => 'Barcelona',
            'time' => "10:40",
            'temperature' => [
                'current' => 26,
                'high' => 26,
                'low' => 16
            ],
            'description' => "Sunny",
        ]
    ]
]);