$(document).ready(function() {
    //Funcion para rescatar los datos json del PHP usando "fetch"
    var informacionTiempo = "http://localhost/pruebaOnestic/api/weather.php";
    var contenido = document.querySelector('#respuesta');

        fetch(informacionTiempo)
        .then(res => res.json())
        .then(data => {
            
            for(let item of data["items"]){ 

                contenido.innerHTML+=` 
                <div class='renderImg widget ${item.city}'/>
                    <div class="caja-time-city">
                        <div class='time'> ${item.time}</div>  
                        <div class='city'> ${item.city}</div>
                    </div>

                    <div class='temp'>
                        <div>${item.temperature.current}º</div>
                    </div>
                    <div class='iconoTiempo'><img class="icono-tiempo" src="assets/icon/${item.description}.svg"/></div>
                
                    <div class="caja-tempmax-desc"> 
                        <div class='description'>${item.description}</div>
                        <div class="temp-high">Max. ${item.temperature.high}º</div>
                        <div class="temp-low">Min. ${item.temperature.low} º</div>
                    </div>
                </div>`;
            }

        }); 
    
});


//Funcion para darkmode de la pagina y cambiar el estado del boton
const btnSwitch = document.querySelector('#switch');
btnSwitch.addEventListener('click', () => {
    document.querySelector(".caja-widgets").classList.toggle('dark');
    document.querySelector(".menu").classList.toggle('dark');
    btnSwitch.classList.toggle('active');
});



//Funcion para cambia el css al clickar en el menu y activar el boton
function toggleTheme(value){

    var estiloLista = "hojalis.css";
    var estiloVista = "hojavis.css";

    var linksCss = document.getElementsByTagName('link');
    var sh = linksCss[0].href;

    if(value == 1){
        linksCss[0].href = "css/"+estiloLista;
        $("#listMode").addClass("activo");
        $("#imgMode").removeClass("activo");
   
        
    }else{
        linksCss[0].href = "css/"+estiloVista;
        $("#listMode").removeClass("activo");
        $("#imgMode").addClass("activo");
    }

}

