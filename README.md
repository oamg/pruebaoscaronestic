He realizado el proyecto con un servidor local (XAMPP) para poder recoger los datos del archivo PHP, lo he desarrollado con
Visual Studio Code y usado los lenguajes y tecnologías JavaScript, Jquery, HTML, CSS, SASS y PHP (el archivo por defecto).

El proyecto consta de un archivo HTML (index.html) donde he desarrollado el esqueleto he importado las librerias necesarias para dar 
el efecto visual y funciones deseados.

El estilo de la página consta de 6 archivos:
1 archivo hojavis.scss que al compilarlo genera el archivo hojavis.css y hojavis.css.map (por defecto) que el fin de dichos
archivos es dar el estilo visual que se pide en el ejercicio. Para ello he colocado los diferentes elementos con flexbox y 
grid ya que me permite controlar y modificar los elementos como mejor me convenga y conseguir un diseño responsivo de la 
mejor manera posible. A parte para el efecto del boton he utilizado SASS y para poder hacer la versión "dark" he implementado
una clase aposta solo para ese fin.

1 archivo hojalis.scss que comparte el mismo proceso que el apartado anterior con la diferencia de que este archivo lo importo
a través de JavaScript, en el HTML solo carga una hoja de estilo (la principal).

1 archivo script.js que implementa tres funciones:

La primera función es leer los datos(json) que contiene el archivo weather.php, una vez los tengo leidos, recorro todos 
los items que contiene y dibujo la estructura donde voy a "imprimir" en un div que lo he llamado "respuesta" 
cada atributo de su item. He de añadir que como no sé
si tenía que subirse a un servidor a parte de Bitbucket he metido en una variable la ruta de mi localhost para que solo se
tenga que modificar esa ruta para que todo funcione o se pueda comprobar, la variable se llama "informacionTiempo"

La segunda funcion simplemente con la ayuda de SASS defino un evento en el boton switch que tengo para que 
al clickarlo con la función toggle me active o no la clase dark (que es el modo oscuro que se solicita)


La tercera función es creada para que en vez de usar dos HTML para cambiar el modo lista o visual cargo las diferentes
ojas de estilo desde un mismo html, simplemente capturo el "link" de css, crea una especie de array y dependiendo el valor
que tenga en ese momento cargará un css o otro. En cada uno de los botones le he asignado un valor y un evento onclick con
la función para que pueda ser llamada.

1 archivo PHP que venía predefinido por la empresa y una serie de imagenes, fuentes e iconos.
Todo lo he ordenado de manera que cada carpeta contiene su nombre de cada extensión de archivos con el fin de tenerlo todo 
mejor estrucutrado.

Comentario:
He de añadir que no he visto bien si el diseño ocupada el 100% de la página o un 60% con margin 0 auto para que respire,
porque en el repositorio podía apreciar un hueco en ambos lados pero en el enlace del README se puede comprobar como ocupa
el 100% y he obtado por hacerlo version expandida y ir retocandolo para adaptarlo a todas las pantallas.
